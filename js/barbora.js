$(document).ready(function(){
	var data = {
  // A labels array that can contain any sort of values
  labels: ['Pirmadienis', 'Antradienis', 'Trečiadienis', 'Ketvirtadienis', 'Penktadienis'],
  // Our series array that contains series objects or in this case series data arrays
  series: [
    [5,4,3,2,1]
  ]
};

// Create a new line chart object where as first parameter we pass in a selector
// that is resolving to our chart container element. The Second parameter
// is the actual data object.
new Chartist.Line('.ct-chart', data);
})