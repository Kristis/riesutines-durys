$(document).ready(function(){$(".button-collapse").sideNav();});

$(document).ready(function(){	
	var data = {
  // A labels array that can contain any sort of values
  labels: ['1 sav', '2 sav', '3 sav', '4 sav', '5 sav', '6 sav', 'ŠIANDIEN :)'],
  // Our series array that contains series objects or in this case series data arrays
  series: [
    [3, 4, 5, 4, 2, 4, 5]
  ]
};

// Create a new line chart object where as first parameter we pass in a selector
// that is resolving to our chart container element. The Second parameter
// is the actual data object.
new Chartist.Line('.ct-chart', data);

});

